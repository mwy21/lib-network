package com.mwy.lib_network.model

import androidx.annotation.Keep


import com.google.gson.Gson
import com.mwy.lib_network.di.NetworkManager

@Keep
data class Resource<out T>(val status: Status, val data: T?, val message: String?) {

    enum class Status {
        SUCCESS,
        ERROR
    }

    companion object {
        fun <T> success(data: T): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(message: String, data: T? = null): Resource<T> {
            var errorModel: ErrorBodyModel? = null
            try {
                errorModel = message.run {
                    Gson().fromJson(this, ErrorBodyModel::class.java)
                } ?: ErrorBodyModel()
                errorModel.run {
                    if (error == "invalid_token" || error == "unauthorized") {
                        // TODO:  待优化位置
                        NetworkManager.onTokenExpired()
//                        AppHook.get().currentActivity()?.let {
//                            LoginActivity.launch(it)
//                        }
                    }
                }
            } catch (e: Exception) {
            } finally {
                var msg = errorModel?.error ?: message
                if (msg.uppercase().contains("FAILED TO CONNECT")) {
                    msg = "网络连接失败，请检查您的网络连接情况"
                }
                if (!msg.contains("invalid_token") && !msg.contains("unauthorized")) {
                    //                    ToastUtil.showText(msg)
                }

                return Resource(Status.ERROR, data, msg)
            }


        }
    }
}