package com.mwy.lib_network.model

import androidx.annotation.Keep

@Keep
data class ErrorBodyModel(
    val error: String? = "",
    val message: String? = "",
    val success: Boolean? = false,
    val error_description: String? = ""
)