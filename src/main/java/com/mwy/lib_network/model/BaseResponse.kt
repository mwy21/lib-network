package com.mwy.lib_network.model

import androidx.annotation.Keep

@Keep
abstract class BaseResponse<T> {

    abstract val success:Boolean

    abstract val data: T?

    abstract val code: String?

    abstract val msg:String?

}