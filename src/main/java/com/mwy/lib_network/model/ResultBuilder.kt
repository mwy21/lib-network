package com.mwy.lib_network.model

import androidx.annotation.Keep
import com.mwy.lib_network.base.RequestException
@Keep
class ResultBuilder<T> {
    var onSuccess: (data: T?) -> Unit = {}
    var onDataEmpty: () -> Unit = {}
    var onFailed: (errorCode: String?, errorMsg: String?) -> Unit = { _, _ -> }
    var onError: (e: RequestException?) -> Unit = { e -> }
    var onComplete: () -> Unit = {}
}