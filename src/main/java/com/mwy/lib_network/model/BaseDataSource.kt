package com.mwy.lib_network.model



import androidx.annotation.Keep
import com.mwy.lib_network.util.Tools


import retrofit2.Response
@Keep
abstract class BaseDataSource {

    protected suspend fun <T> getResult(call: suspend () -> Response<T>): Resource<T> {
        try {

            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                Tools.logUtils("tag", "请求 Successful:$body")
                if (body != null) {
                    return Resource.success(body)
                }
            }
            var errorMsg = ""
            if (!response.isSuccessful) {
                errorMsg = response.errorBody()?.string() ?: ""
            }

            return error(errorMsg)
//            return error(" ${response.code()} ${response.message()}")
        } catch (e: Exception) {
            Tools.logUtils("tag", "请求异常:${e.toString()}")
            var showMsg=e.message ?: e.toString()
            if (showMsg.uppercase().contains("FAILED TO CONNECT"))
                showMsg="网络连接失败，请检查您的网络连接情况"
            return error(showMsg)
        }
    }

    private fun <T> error(message: String): Resource<T> {
        Tools.logUtils("tag", message)
        return Resource.error(message)
    }

    private fun <T> error(message: String, data: T? = null): Resource<T> {
        Tools.logUtils("tag", message)
        return Resource.error(message, data)
    }
}