package com.mwy.lib_network.model

import androidx.annotation.Keep
import com.mwy.lib_network.base.RequestException
import com.google.gson.annotations.SerializedName

@Keep
open class ApiResponse<T>(
    override val data: T? = null,
    override val code: String?= null,
    @SerializedName("message")
    override val msg: String? = null,
    open val exception: RequestException? = null,
) : BaseResponse<T>() {

    override val success: Boolean
        get() = code == "0"
}

class StartResponse<T> : ApiResponse<T>()

data class SuccessResponse<T>(override val data: T) : ApiResponse<T>(data)

class EmptyResponse<T> : ApiResponse<T>()

data class FailureResponse<T>(override val exception: RequestException) :
    ApiResponse<T>(exception = exception)

