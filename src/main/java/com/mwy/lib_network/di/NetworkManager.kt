package com.mwy.lib_network.di

import androidx.annotation.Keep
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mwy.lib_network.AuthorizationInterceptor
import com.mwy.lib_network.constant.RequestUrl

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


@Keep
object NetworkManager {

    private val loggingInterceptor = HttpLoggingInterceptor()


    private fun myHttpClient(): OkHttpClient {
        val builder = OkHttpClient().newBuilder()
            .addInterceptor(loggingInterceptor.setLevel(if (RequestUrl.isDebug) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE))
//            .addInterceptor(loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY))
            .addInterceptor(AuthorizationInterceptor())
//            .addInterceptor(TokenInterceptor())  //Token拦截器
        return builder.build()
    }


    private fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(RequestUrl.BASE_URL)
        .client(myHttpClient())
        .addConverterFactory(GsonConverterFactory.create(provideGson()))
        .build()


    fun provideGson(): Gson = GsonBuilder().create()

    fun <T> provideService(service: Class<T>): T = provideRetrofit().create(service)

    var onTokenExpired:()-> Unit={

    }

}
