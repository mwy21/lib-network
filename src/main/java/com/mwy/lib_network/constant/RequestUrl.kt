package com.mwy.lib_network.constant

import com.mwy.lib_network.BuildConfig

/**
 * Describe:
 * Author: mwy
 * Date: 2022/11/28.
 */
object RequestUrl {

    var BASE_URL = ""

    var TOKEN_VALUE=""

    var isDebug=BuildConfig.isDebug
}