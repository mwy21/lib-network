package com.mwy.lib_network.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mwy.lib_network.handleException
import com.mwy.lib_network.model.ApiResponse
import com.mwy.lib_network.model.BaseResponse
import com.mwy.lib_network.model.FailureResponse
import com.mwy.lib_network.model.ResultBuilder
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

fun <T> ViewModel.launchWithLoadingAndCollect(
    block: suspend () -> ApiResponse<T>,
    listenerBuilder: ResultBuilder<T>.() -> Unit
) {
    viewModelScope.launch {
        launchWithLoadingGetFlow(block).collect { response ->
            parseResultAndCallback(response, listenerBuilder)
        }
    }
}

private fun <T> parseResultAndCallback(
    response: BaseResponse<T>,
    listenerBuilder: ResultBuilder<T>.() -> Unit
) {
    val listener = ResultBuilder<T>().also(listenerBuilder)

    when(response){
        is FailureResponse ->listener.onError(response.exception)
        else->{
            if (response.success)
                listener.onSuccess(response.data)
            else listener.onFailed(response.code,response.msg)
        }
    }

    listener.onComplete()
}

fun <T> launchWithLoadingGetFlow(block: suspend () -> ApiResponse<T>): Flow<ApiResponse<T>> {
    return flow {
        this.emit(block())
    }.onStart {

    }.onCompletion {

    }.catch {
        emit(FailureResponse(handleException(it)))
    }
}