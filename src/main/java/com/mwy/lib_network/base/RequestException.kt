package com.mwy.lib_network.base

import androidx.annotation.Keep
import com.mwy.lib_network.enum.HttpError
import com.mwy.lib_network.model.BaseResponse

@Keep
data class RequestException(
    val code: String? = "",
    val errorMsg: String? = "",
    val error: String? = null
) : Exception() {

    constructor(response: BaseResponse<*>) : this(
        response.code,
        response.msg,
        response.msg
    )

    constructor(httpError: HttpError, error: String?) : this(
        httpError.code,
        httpError.message,
        error
    )

}