package com.mwy.lib_network.util

import android.util.Log
import com.mwy.lib_network.BuildConfig

/**
 * Describe:
 * Author: mwy
 * Date: 2022/11/28.
 */
object Tools {
    @JvmStatic
    fun logUtils(tag: String?, msg: String) {
        if (BuildConfig.isDebug) {
            Log.e(tag, msg)
        }
    }
}