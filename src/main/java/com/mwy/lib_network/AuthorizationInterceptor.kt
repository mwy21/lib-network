package com.mwy.lib_network

import android.text.TextUtils
import androidx.annotation.Keep
import com.mwy.lib_network.constant.RequestUrl

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
@Keep
class AuthorizationInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
//        val response = if (TextUtils.isEmpty(Constants.TOKEN_VALUE)) {
//            val originalRequest = chain.request()
//            val updateRequest =
//                originalRequest.newBuilder().header("Authorization", Constants.TOKEN_BASE).build()
//            chain.proceed(updateRequest)
//        } else {
//            val originalRequest = chain.request()
//            val updateRequest = originalRequest.newBuilder()
//                .header("Authorization", Constants.TOKEN_BEARER + Constants.TOKEN_VALUE).build()
//            chain.proceed(updateRequest)
//        }
//        Timber.v("请求返回code：${response.code}")
//        Timber.v("请求返回message：${response.message}")
//        when (response.code) {
//            401 -> {
//                Timber.v("401异常信息:${response}")
//            }
//        }
        return try {

            if (TextUtils.isEmpty(RequestUrl.TOKEN_VALUE)) {
                val originalRequest = chain.request()
//                val updateRequest =
//                    originalRequest.newBuilder().header("Authorization", Constants.TOKEN_BASE).build()

                chain.proceed(originalRequest)
            } else {
                val originalRequest = chain.request()
                val updateRequest = originalRequest.newBuilder()
                    .header("Authorization", RequestUrl.TOKEN_VALUE).build()

                chain.proceed(updateRequest)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return chain.proceed(chain.request())
        }
    }

    /**
     * 将chain.proceed处理中发生的Throwable包装成IOExceptionWrapper
     */
    class IOExceptionWrapper(message: String?, cause: Throwable?) : IOException(message, cause)
}

