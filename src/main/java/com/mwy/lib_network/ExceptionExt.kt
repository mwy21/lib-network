package com.mwy.lib_network

import androidx.annotation.Keep
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.mwy.lib_network.model.ErrorBodyModel
import com.mwy.lib_network.base.RequestException
import com.mwy.lib_network.enum.HttpError

import retrofit2.HttpException
import java.net.UnknownHostException
@Keep
fun handleException(throwable: Throwable) = when (throwable) {
    is UnknownHostException -> RequestException(HttpError.NETWORK_ERROR, throwable.message)
    is HttpException -> {
        var errorBodyModel: ErrorBodyModel?= ErrorBodyModel(error = throwable.message, message = throwable.message)
        try {
            errorBodyModel = throwable.response()?.errorBody()?.string()?.run {
                Gson().fromJson(this, ErrorBodyModel::class.java)
            }
            errorBodyModel?.error?.let {msg->
                if (msg=="invalid_token"||msg=="unauthorized"){
// TODO: 待修改

//                    AppHook.get().currentActivity()?.let {
//                        LoginActivity.launch(it)
//                        it.finish()
//                    }
                }

            }
        }catch (e:Exception){
            if (throwable.message!=null){
                if (throwable.message!!.uppercase().contains("FAILED TO CONNECT")){
                    errorBodyModel= ErrorBodyModel(error = "网络连接失败，请检查您的网络连接情况", message = "网络连接失败，请检查您的网络连接情况")
                }
//                ToastUtil.showText("网络连接失败，请检查您的网络连接情况")
            }else{
                errorBodyModel= ErrorBodyModel(error = throwable.message, message = throwable.message)
            }

        }
        RequestException(errorMsg = errorBodyModel!!.message, error = errorBodyModel.error)

    }
    is JsonParseException -> RequestException(HttpError.JSON_PARSE_ERROR, throwable.message)
    is RequestException -> throwable
    else -> RequestException(HttpError.UNKNOWN, throwable.message)
}
