package com.mwy.lib_network.enum

import androidx.annotation.Keep

@Keep
enum class HttpError(val code: String, val message: String){
    UNKNOWN((-100).toString(),"未知错误"),
    NETWORK_ERROR(1000.toString(), "网络连接超时，请检查网络"),
    JSON_PARSE_ERROR(1001.toString(), "Json 解析失败")
    //······
}